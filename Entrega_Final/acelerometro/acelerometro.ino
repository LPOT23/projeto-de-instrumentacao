#include <Wire.h>
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>

Adafruit_MPU6050 mpu;

int16_t ax, ay, az;
float somador = 0.0;

void setup() {
  Serial.begin(115200);
  Wire.begin();
  
  if (!mpu.begin()) {
    Serial.println("Não foi possível iniciar o sensor MPU6050");
    while (1);
  }

  Serial.println("Sensor iniciado");
}

void loop() {
  Serial.println("Próximo\n");
  delay(5000);
  
  for (int i = 0; i < 100; i++) {
    sensors_event_t a, g, temp;
    mpu.getEvent(&a, &g, &temp);
    
    ax = a.acceleration.x;
    ay = a.acceleration.y;
    az = a.acceleration.z;

    float accel_ang_y = atan(ax / sqrt(pow(ay, 2) + pow(az, 2))) * (180.0 / PI);
    // float accel_ang_x = atan(ay / sqrt(pow(ax, 2) + pow(az, 2))) * (180.0 / PI);
    
    Serial.print("Inclinação Y:");
    Serial.println(accel_ang_y);
    
    somador += accel_ang_y;
    delay(10);
  }

  
  Serial.print("Média de Inclinação Y:");
  Serial.println(somador / 100);
  somador = 0.0;
}

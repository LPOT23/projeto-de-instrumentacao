#include <Wire.h>
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>

Adafruit_MPU6050 mpu;

int gx, gy, gz;

long tempo_prev, dt;
float girosc_ang_x, girosc_ang_y, somador = 0.0;
float girosc_ang_x_prev, girosc_ang_y_prev;

void setup() {
  Serial.begin(115200);
  Wire.begin();
  
  if (!mpu.begin()) {
    Serial.println("Não foi possível iniciar o sensor MPU6050");
    while (1);
  }

  Serial.println("Sensor iniciado corretamente");
  tempo_prev = millis();
}

void loop() {
  Serial.println("Mova para a próxima posição");
  delay(5000);

  for (int i = 0; i < 100; i++) {
    sensors_event_t a, g, temp;
    mpu.getEvent(&a, &g, &temp);

    gx = g.gyro.x;
    gy = g.gyro.y;
    gz = g.gyro.z;

    dt = millis();
    // tempo_prev = millis();

    girosc_ang_x = (gx / 131.0) * dt / 1000.0 + girosc_ang_x_prev;
    // girosc_ang_y = (gy / 180.0) * dt / 1000.0 + girosc_ang_y_prev;

    girosc_ang_x_prev = girosc_ang_x;
    girosc_ang_y_prev = girosc_ang_y;

    Serial.print("Rotação em Y: ");
    Serial.println(girosc_ang_y);

    somador += girosc_ang_y;
    delay(100);
  }

  Serial.print("Média de Rotação em Y: ");
  Serial.println(somador / 100);
  somador = 0.0;
}

# Projeto de Instrumentação

Equipe: Luiz Phelipe de Oliveira Tavares

Matrícula: 190092084

## Projeto MPU-6050: Instrumentação em Eletrônica 

> Este repositório abrange os códigos desenvolvidos para os pontos de controle da disciplina "Instrumentação em Eletrônica". O primeiro ponto de controle se dedica à coleta de dados do acelerômetro e giroscópio. O segundo ponto de controle aborda a calibração do acelerômetro, enquanto a entrega final concentra-se na medição do ângulo de inclinação utilizando os dados do acelerômetro e giroscópio.